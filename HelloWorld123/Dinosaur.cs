﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld123
{
    //this class contains dinosaur properties
    class Dinosaur1
    {
       
            //four things are required to show case a dinosaur.
            //name
            public string DinoName { set; get; }
            //height
            public int DinoHeight { set; get; }
            //weight
            public int DinoWeight { set; get; }
            //terrain
            public string DinoTerrain { set; get; }

        public int DinoID { set; get; }

        //TODO

        //default constructor

        public Dinosaur1()
            {
                DinoName = "some name";
                DinoHeight = 100;
                DinoWeight = 100;
                DinoTerrain = "some terrain";
           
            }
        //custom constructor with parameters
       


        public Dinosaur1(string DinoName1, int DinoHeight1, int DinoWeight1, string DinoTerrain1, int DinoID1)
            {
                DinoName = DinoName1;
                DinoHeight = DinoHeight1;
                DinoWeight = DinoWeight1;
                DinoTerrain = DinoTerrain1;
                DinoID = DinoID1;
            }
        //function do take input for Dino
        public Dinosaur1 Input()
        {
            //some temporary values to which we will fill user entered values.
            string tempDinoName = "Triceratops";
            int tempDinoHeight = 10;
            int tempDinoWeight = 12;
            string tempDinoTerrain = "North America";
            int tempDinoID = 1;

            //collect input from user

            var message = "";

            message = "enter dino name";
            Console.WriteLine(message);
            tempDinoName = Console.ReadLine();

            message = "enter dino height";
            Console.WriteLine(message);
            var tempNumber = Console.ReadLine();
            tempDinoHeight = Convert.ToInt32(tempNumber);

            message = "enter dino weight";
            Console.WriteLine(message);
            var tempNumber2 = Console.ReadLine();
            tempDinoWeight = Convert.ToInt32(tempNumber2);

            message = "enter dino terrain";
            Console.WriteLine(message);
            tempDinoTerrain = Console.ReadLine();

            message = "enter dino id";
            Console.WriteLine(message);
            var tempNumber3 = Console.ReadLine();
            tempDinoID = Convert.ToInt32(tempNumber3);

            //fill it up to a dino object.
            //create a dino object to return after getting all the values from the user. 

            var toReturnDino = new Dinosaur1(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            return toReturnDino;
        }
        //function to display Dino

        public void display()
        {
            
                Console.WriteLine("Dino - " + DinoName + " Details");
            Console.WriteLine("Id - " + DinoID);
            Console.WriteLine("Weight  " + DinoWeight);
                Console.WriteLine("Height " + DinoHeight);
                Console.WriteLine("Terrain " + DinoTerrain);
                Console.WriteLine("------------");
            

        }


        //function to print all Dino Names
    }
    
}

